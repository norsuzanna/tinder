"use client";

import React, { useState, useEffect } from 'react';
import Card from './Card';
import { User } from '../models/User';
import { fetchUsers } from '../services/userService';

const CardDeck: React.FC = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const loadUsers = async () => {
      const fetchedUsers = await fetchUsers();
      setUsers(fetchedUsers);
    };

    loadUsers();
  }, []);

  const handleSwipe = (direction: string) => {
    if (direction === 'right') {
      console.log('Liked:', users[currentIndex].name);
    } else {
      console.log('Disliked:', users[currentIndex].name);
    }

    setCurrentIndex((prevIndex) => prevIndex + 1);
  };

  return (
    <div className="flex justify-center items-center h-screen bg-gray-100">
      {currentIndex < users.length && (
        <Card user={users[currentIndex]} onSwipe={handleSwipe} />
      )}
    </div>
  );
};

export default CardDeck;
