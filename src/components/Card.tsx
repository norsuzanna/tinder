import React from 'react';
import { User } from '../models/User';

interface CardProps {
    user: User;
    onSwipe: (direction: string) => void;
}

const Card: React.FC<CardProps> = ({ user, onSwipe }) => {
    return (
        <div className="bg-white p-6 rounded-lg shadow-lg text-center">
            <img className="w-full h-64 object-cover rounded-t-lg" src={user.imageUrl} alt={user.name} />
            <div className="mt-4">
                <h2 className="text-2xl text-purple-700 font-bold">{user.name}, {user.age}</h2>
                <p className="mt-2 text-gray-600">{user.bio}</p>
            </div>
            <div className="flex justify-around mt-4">
                <button
                    className="bg-red-500 text-white py-2 px-4 rounded"
                    onClick={() => onSwipe('left')}
                >
                    Dislike
                </button>
                <button
                    className="bg-green-500 text-white py-2 px-4 rounded"
                    onClick={() => onSwipe('right')}
                >
                    Like
                </button>
            </div>
        </div>
    );
};

export default Card;
