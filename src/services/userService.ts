import { User } from "../models/User";

export const fetchUsers = async (): Promise<User[]> => {
    return [
        {
            id: 1,
            name: "Alice",
            age: 25,
            bio: "Loves hiking and outdoor adventures.",
            imageUrl: "https://picsum.photos/id/237/200/300",
        },
        {
            id: 2,
            name: "Bob",
            age: 30,
            bio: "Enjoys painting and sculpting.",
            imageUrl: "https://picsum.photos/seed/picsum/200/300",
        },
        {
            id: 3,
            name: "Lily",
            age: 21,
            bio: "Enjoys painting and sculpting.",
            imageUrl: "https://fastly.picsum.photos/id/40/4106/2806.jpg?hmac=MY3ra98ut044LaWPEKwZowgydHZ_rZZUuOHrc3mL5mI",
        },
        {
            id: 4,
            name: "Rose",
            age: 40,
            bio: "Enjoys painting and sculpting.",
            imageUrl: "https://fastly.picsum.photos/id/39/3456/2304.jpg?hmac=cc_VPxzydwTUbGEtpsDeo2NxCkeYQrhTLqw4TFo-dIg",
        },
        {
            id: 5,
            name: "Joe",
            age: 35,
            bio: "Enjoys painting and sculpting.",
            imageUrl: "https://fastly.picsum.photos/id/91/3504/2336.jpg?hmac=tK6z7RReLgUlCuf4flDKeg57o6CUAbgklgLsGL0UowU",
        }
    ];
};
