import CardDeck from "@/components/CardDeck";

export default function Home() {
  return (
    <div className="App">
      <header className="bg-blue-600 text-white p-4">
        <h1 className="text-3xl font-bold">Tinder Recommendation</h1>
      </header>
      <main>
        <CardDeck />
      </main>
    </div>
  );
};
